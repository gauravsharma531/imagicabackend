var mongoose = require('mongoose');
console.log("sesrvices");

var userModel = mongoose.model('user');

var signUp = function(reqModel,callback){
	var user = new userModel(reqModel);
	console.log(user)
	user.save(function(err,res){
		callback(err,res);
	});

}

var listUsers = function(reqModel,callback)
{
	userModel.listUsers(reqModel, callback);
}

var deleteUserById = function(reqModel,callback)
{
	userModel.deleteUser(reqModel,callback);
}

var updateUser = function(reqModel, callback){
	userModel.updateUser(reqModel, callback);
}
module.exports.signUp = signUp;
module.exports.listUsers = listUsers;
module.exports.deleteUserById = deleteUserById;
module.exports.updateUser = updateUser;

