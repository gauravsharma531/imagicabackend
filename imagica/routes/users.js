var express = require('express');
// var router = express.Router();
var reg = require('../services/registration');
var router = express.Router();

/* GET users listing. */
router.get('/user', function(req, res, next) {
  next('USER_ERROR');
});

router.post('/signup',function(req,res,next){
	// res.send("user registered");
	var User = {};
	User.userName = req.body.userName;
	User.pwd = req.body.pwd;
	reg.signUp(User,function(err,result)
	{
		if(err)
			res.send("not registered");
		else
			res.send(result);
	})

});

router.get('/users', function(req, res, next) {
		var condtions = {};
		reg.listUsers(condtions, function(err,usersList){
			console.log(err,usersList.length, usersList);
			if(err){
				res.send(err);
			} else {
				res.send(usersList);
			}
		});
	  //res.send({ title: 'users' });
	});

router.get('/deleteUser1',function(req,res,next)
{
	console.log(" deleteUser ");
	var condtions = {"_id":req.query.id};
	console.log(req.query);
	reg.deleteUserById(condtions,function (err,response)
	{
		if(err)
		{
			res.send(err)
		}
		else
		{
			res.send(req.query.id + " deleted successfully");
		}
	});
});


router.get('/updateUser',function(req,res,next)
{
	console.log(" updateuser ");
	var condtions = {"id":req.query.id, "name" : req.query.name};
	console.log(req.query);
	reg.updateUser(condtions,function (err,response)
	{
		if(err)
		{
			res.send(err)
		}
		else
		{
			res.send(req.query.id + " updated successfully");
		}
	});
});

module.exports = router;
