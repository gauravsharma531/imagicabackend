var express = require('express');

var router = express.Router();
var userRouter = require('./users');


function init (app){
	router.get('/', function(req, res, next) {
	  res.send({ title: 'Express' });
	});

	app.use('/users',userRouter);
	app.use('/',router);
}


module.exports.init = init;
