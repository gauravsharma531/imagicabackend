var mongoose = require('mongoose');
console.log("model");

var userSchema = {
	
	userName: {type: String, unique: true},
	pwd:{type: String}
};

var userModel = new mongoose.Schema(userSchema);
userModel.static({
	listUsers : function(options, callback){
		this.find(options, callback);
	},
	deleteUser : function(options,callback)
	{
		this.remove(options,callback);
	},
	updateUser : function(options, callback){
		this.update({_id : options.id}, {userName : options.name} , callback);
	}

})
mongoose.model("user",userModel);



